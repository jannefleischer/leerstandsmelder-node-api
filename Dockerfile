FROM node:8.17.0

ADD . ./leerstandsmelder-node-api

#WORKDIR leerstandmelder-node-api
RUN npm install leerstandsmelder-node-api/ && \
    leerstandsmelder-node-api/bin/setup.js && \
    node leerstandsmelder-node-api/app.js && \
    tail -f /dev/null
